<?php

namespace App\Exceptions;

use Exception;

class MidtransException extends Exception
{
    /**
     * Codes:
     * 1 = Data validation error
     * 2 = Payment type to found
     */
}
