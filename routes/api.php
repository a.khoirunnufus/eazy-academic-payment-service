<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('payment/charge', 'App\Http\Controllers\PaymentController@chargeTransaction');
Route::post('payment/cancel', 'App\Http\Controllers\PaymentController@cancelTransaction');
Route::post('payment/midtrans-notification-callback', 'App\Http\Controllers\PaymentController@midtransNotificationCallback');
