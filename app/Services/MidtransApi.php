<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Exceptions\MidtransException;
use Carbon\Carbon;

class MidtransApi {

    private $base_url;
    private $req_header;

    public function __construct()
    {
        $auth_key = base64_encode(config('midtrans.MIDTRANS_SERVER_KEY') . ':');
        $this->base_url = config('midtrans.MIDTRANS_SERVER_URL');
        $this->req_header = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . $auth_key
        ];
    }

    public function chargeTransaction($data)
    {
        $response = null;

        if ($data['payment_type'] == 'bca_va') {
            $response = $this->chargeBca($data);
        }
        elseif ($data['payment_type'] == 'mandiri_bill_payment') {
            $response = $this->chargeMandiri($data);
        }
        elseif ($data['payment_type'] == 'bni_va') {
            $response = $this->chargeBni($data);
        }
        else {
            $message = "Payment Type Not Found";
            self::createLog('', 'error', json_encode($data), $message);
            throw new MidtransException('Payment type not found!', 2);
        }

        $return_data = [
            'status_code' => $response->status_code,
            'status_message' => $response->status_message,
            'transaction_id' => $response->transaction_id,
            'transaction_exp' => $response->expiry_time,
        ];

        if (in_array($data['payment_type'], ['bca_va', 'bni_va'])) {
            $return_data = array_merge($return_data, [
                'va_number' => $response->va_numbers[0]->va_number,
            ]);
        }
        elseif ($data['payment_type'] == 'mandiri_bill_payment') {
            $return_data = array_merge($return_data, [
                'bill_key' => $response->bill_key,
                'biller_code' => $response->biller_code,
            ]);
        }

        return $return_data;
    }

    private function chargeBca($data)
    {
        $name_arr = explode(' ', $data['name']);
        $customer_name = [
            'first_name' => array_shift($name_arr),
            'last_name' => implode(' ', $name_arr),
        ];

        $req_body = [
            'payment_type' => 'bank_transfer',
            'transaction_details' => [
                'order_id' => $data['order_id'],
                'gross_amount' => $data['amount_total'],
            ],
            'bank_transfer' => [
                'bank' => 'bca',
            ],
            "custom_expiry" => [
                "order_time" => Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s O'),
                "expiry_duration" => $data['exp_time'],
                "unit" => 'minute'
            ],
            "customer_details" => [
                "email" => $data['email'],
                "first_name" => $customer_name['first_name'],
                "last_name" => $customer_name['last_name'],
                "phone" => $data['phone']
            ],
            'item_details' => [],
        ];

        foreach ($data['item_details'] as $key => $item) {
            $req_body['item_details'][$key] = [
                "id" => (string) Str::uuid(),
                "price" => $item['price'],
                "quantity" => 1,
                "name" => $item['name']
            ];
        }

        Log::debug([
            'request' => [
                'url' => $this->base_url.'/charge',
                'header' => $this->req_header,
                'body' => $req_body,
            ]
        ]);

        $response = Http::withHeaders($this->req_header)
            ->post($this->base_url.'/charge', $req_body)
            ->object();

        Log::debug([
            'response' => $response,
        ]);

        return $response;
    }

    private function chargeMandiri($data)
    {
        $name_arr = explode(' ', $data['name']);
        $customer_name = [
            'first_name' => array_shift($name_arr),
            'last_name' => implode(' ', $name_arr),
        ];

        $req_body = [
            'payment_type' => 'echannel',
            'transaction_details' => [
                'order_id' => $data['order_id'],
                'gross_amount' => $data['amount_total'],
            ],
            'echannel' => [
                'bill_info1' => 'Payment:',
                'bill_info2' => 'Purchase at Eazy Academic Application',
            ],
            "custom_expiry" => [
                "order_time" => Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s O'),
                "expiry_duration" => $data['exp_time'],
                "unit" => 'minute'
            ],
            "customer_details" => [
                "email" => $data['email'],
                "first_name" => $customer_name['first_name'],
                "last_name" => $customer_name['last_name'],
                "phone" => $data['phone']
            ],
            'item_details' => [],
        ];

        foreach ($data['item_details'] as $key => $item) {
            $req_body['item_details'][$key] = [
                "id" => (string) Str::uuid(),
                "price" => $item['price'],
                "quantity" => 1,
                "name" => $item['name']
            ];
        }

        Log::debug([
            'request' => [
                'url' => $this->base_url.'/charge',
                'header' => $this->req_header,
                'body' => $req_body,
            ]
        ]);

        $response = Http::withHeaders($this->req_header)
            ->post($this->base_url.'/charge', $req_body)
            ->object();

        Log::debug([
            'response' => $response,
        ]);

        return $response;
    }

    private function chargeBni($data)
    {
        $name_arr = explode(' ', $data['name']);
        $customer_name = [
            'first_name' => array_shift($name_arr),
            'last_name' => implode(' ', $name_arr),
        ];

        $req_body = [
            'payment_type' => 'bank_transfer',
            'transaction_details' => [
                'order_id' => $data['order_id'],
                'gross_amount' => $data['amount_total'],
            ],
            'bank_transfer' => [
                'bank' => 'bni',
            ],
            "custom_expiry" => [
                "order_time" => Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s O'),
                "expiry_duration" => $data['exp_time'],
                "unit" => 'minute'
            ],
            "customer_details" => [
                "email" => $data['email'],
                "first_name" => $customer_name['first_name'],
                "last_name" => $customer_name['last_name'],
                "phone" => $data['phone']
            ],
            'item_details' => [],
        ];

        foreach ($data['item_details'] as $key => $item) {
            $req_body['item_details'][$key] = [
                "id" => (string) Str::uuid(),
                "price" => $item['price'],
                "quantity" => 1,
                "name" => $item['name']
            ];
        }

        Log::debug([
            'request' => [
                'url' => $this->base_url.'/charge',
                'header' => $this->req_header,
                'body' => $req_body,
            ]
        ]);

        $response = Http::withHeaders($this->req_header)
            ->post($this->base_url.'/charge', $req_body)
            ->object();

        Log::debug([
            'response' => $response,
        ]);

        return $response;
    }

    public function cancelTransaction($data)
    {
        $response = Http::withHeaders($this->req_header)
            ->post($this->base_url.'/'.$data['order_id'].'/cancel')
            ->object();

        return [
            'status_code' => $response->status_code,
            'status_message' => $response->status_message,
        ];
    }

    public static function createLog($type, $status, $data, $comment)
    {
        return DB::table('logs.service_midtrans')->insert([
            'type' => $type,
            'status' => $status,
            'data' => $data,
            'comment' => $comment,
        ]);
    }
}
