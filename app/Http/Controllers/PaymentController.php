<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Services\MidtransApi;
use App\Exceptions\MidtransException;

class PaymentController extends Controller
{
    public function chargeTransaction(Request $request)
    {
        $validated = $request->validate([
            'order_id' => 'required',
            'payment_type' => 'required',
            'amount_total' => 'required|int',
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string',
            'item_details' => 'required|array',
            'item_details.*.name' => 'required|string',
            'item_details.*.price' => 'required|int',
        ]);

        $data = $validated;
        $data['exp_time'] = 24*60; // in minutes

        try {
            $charge_result = (new MidtransApi())->chargeTransaction($data);
            $status = null;
            $payload = null;

            if ($charge_result['status_code'] == "201") {
                $status = 'success';
                MidtransApi::createLog($validated['payment_type'], 'success', json_encode($validated), $charge_result['status_message']);
            } else {
                $status = 'error';
                MidtransApi::createLog($validated['payment_type'], 'error', json_encode($validated), $charge_result['status_message']);
            }

            if (in_array($validated['payment_type'], ['bca_va', 'bni_va'])) {
                $payload = [
                    'transaction_id' => $charge_result['transaction_id'],
                    'va_number' => $charge_result['va_number'],
                    'transaction_exp' => $charge_result['transaction_exp'].'.000 +0700',
                ];
            }
            elseif ($validated['payment_type'] == 'mandiri_bill_payment') {
                $payload = [
                    'transaction_id' => $charge_result['transaction_id'],
                    'bill_key' => $charge_result['bill_key'],
                    'biller_code' => $charge_result['biller_code'],
                    'transaction_exp' => $charge_result['transaction_exp'].'.000 +0700',
                ];
            }

            return response()->json([
                'status' => $status,
                'message' => $charge_result['status_message'],
                'payload' => $payload,
            ], 200);
        }
        catch (\Throwable $ex) {
            return response()->json([
                'status' => 'error',
                'message' => $ex->getMessage(),
            ], 500);
        }
    }

    public function cancelTransaction(Request $request)
    {
        $validated = $request->validate([
            'order_id' => 'required',
        ]);

        $cancel_result = (new MidtransApi())->cancelTransaction($validated);
        $status = null;

        if ($cancel_result['status_code'] == "200") {
            $status = 'success';
            MidtransApi::createLog('cancel_transaction', 'success', json_encode($validated), $cancel_result['status_message']);
        } else {
            $status = 'error';
            MidtransApi::createLog('cancel_transaction', 'error', json_encode($validated), $cancel_result['status_message']);
        }

        return response()->json([
            'status' => $status,
            'message' => $cancel_result['status_message'],
        ], 200);
    }

    public function midtransNotificationCallback(Request $request)
    {
        try {
            DB::beginTransaction();

            $signature = $request->order_id . $request->status_code . $request->gross_amount . config('midtrans.MIDTRANS_SERVER_KEY');
            $signature_key = hash("sha512", $signature);

            if ($signature_key != $request->signature_key) {
                // signature invalid
                throw new \Exception('Signature key not match!');
            }

            if (
                $request->status_code != "200"
                || $request->transaction_status != 'settlement'
                || $request->fraud_status != 'accept'
            ) {
                throw new \Exception('Payload identified as failed by midtrans terms!');
            }

            $bill = DB::table('finance.payment_re_register_bill')
                ->where('prrb_order_id', $request->order_id)
                ->first();

            if (!$bill) {
                throw new \Exception('Bill not found!');
            }

            DB::table('finance.payment_re_register_bill')
                ->where('prrb_id', $bill->prrb_id)
                ->update([
                    'prrb_status' => 'lunas',
                    'prrb_paid_date' => $request->settlement_time.'.000 +0700',
                ]);

            $not_yet_paid_off = DB::table('finance.payment_re_register_bill')
                ->where('prr_id', $bill->prr_id)
                ->where('prrb_status', 'belum lunas')
                ->exists();

            if (!$not_yet_paid_off) {
                DB::table('finance.payment_re_register')
                    ->where('prr_id', $bill->prr_id)
                    ->update([
                        'prr_status' => 'lunas'
                    ]);
            }

            MidtransApi::createLog('webhook', 'success', $request->getContent(), $request->status_message);

            DB::commit();
        }
        catch (\Exception $ex) {
            DB::rollback();
            MidtransApi::createLog('webhook', 'error', $request->getContent(), $ex->getMessage());
        }

        return response()->json(['status' => 'ok'], 200);
    }
}
